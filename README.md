# Welcome to Awesome Project

## Features

- An awesome landing page
- An invitation for download the app
- A contact form
- Another great feature

## Tech Stack

- Lisp
- Smalltalk
- Elm
- ReasonML
- Docker
- Linux
- nginx

## Collaborators

- José Juan Reyes Zuñiga
- Lia Zoe
- Guie Cruz
- Carlos Gibrán
- Eliot Rodrigo
